José Andrés Pizarro
===================

Big Data developer

Bio
---

 * born on 11/28/1982 in Cuenca, Spain
 * EU-citizen
 * email: j.andres.pizarro@gmail.com
 * languages:
   * español
   * english
 * programming skills:
   * Java
   * Python
   * Shell
   * MySQL
   * MongoDB
   * Javascript
   * Eclipse RCP
   * SWT
   * AWT
   * Swing
   * CSS3
   * HTML5
   * Apache Spark
 * Other technical skills:
   * OS: Windows, Linux
   * Hardware devices
   * Drivers development
   * Office suite
   * LaTeX

Motivation
----------

I completed my degree in Computer Science in Madrid in 2007. My MSc. thesis was based on 
the creation, from the requirements elicitation to the implementation of a system for the selection of candidatures for an
Erasmus Mundus Master managed by the Software Engineering group at UPM University.

After this, my first professional working experience took place in 2007 and found soon a motivating work 
at GMV, located in Madrid. From the very beginning I started to develop software for hifly Satellite Monitoring Center,
belonging to a team of more than 80 people that lead us to be top Satellite Control Centers provider company. 
Our customers are the most relevant companies in the industry such as Eutelsat, SES Astra or NBN, among others.

Part of my working experience took place in Luxembourg, working as a software consultant for SES Astra.
This wonderful experience allowed me to work in an international team, where a system for automating satellite control
was developed between 2009 and 2010. Nowadays this system is becoming widely adopted for other companies for automating
control tasks.

From 2018, I joined DATIO towards initiating my career as a Big Data engineer, leading the set up of Python-based
solutions.

Along these years, I have become a technical reference in my working team, specially in anything related
to user experience, and development using Java and Python technologies.

Education
---------

#### 2000--2005 : BSc. + MSc. in **Computer Science Engineering**
_UPM, Madrid, Spain_

Real-time development, project engineering, software engineering, operating systems

Working Experience
------------------

#### 2007-2009: Software Engineer at GMV Aerospace and Defence S.A.U.
_Madrid, Spain_

Junior Software engineer in charge of developing and mantaining GMV's spacecraft monitoring tool codenamed hiflyViews, based on the eclipse RCP platform.
This tool aims to perform real time monitoring of satellites based on received telemetry. Visualization of telemetry is done using different formats,
from tables to scatter graphs and synoptic representations.


#### 2009-2010: Software Engineer at SES Astra
_Betzdorf, Luxembourg_

Software engineer in charge of designing and developing a satellite automation tool codenamed SPELL (https://sourceforge.net/projects/spell-sat/).
This automation suite consists on a server implemented in Python an client implemented using eclipse RCP.
Control automation is based on python modules that include a set of domain-specific primitives that allows monitoring and interacting over the
spacecraft status.


#### 2011-2014: Software Engineer at GMV Aerospace and Defence S.A.U.
_Madrid, Spain_

Software engineer leading different software projects involving Java and Python technologies, focusing on tasks mainly related to user interfaces.
Main projects I have worked include the adaptation of SPELL suite to Eutelsat automation needs in the scope of project NeoFoX, and enhancements to
their Satellite Control Center, from its core features to the development of new tools.


#### 2015-2018: Project Manager at GMV Aerospace and Defence S.A.U.
_Madrid, Spain_

Software engineer leading the evolution of critical infrastucture access system for Eutelsat telecommunications company, codenamed BioLock. This system
grants access to satellite control infrastructures based on biometric authentications and presence detection.
This projects involves different sets of technologies, from hardware devices to web application containers.

#### From 2018: Big Data Engineer at Datio
_Madrid, Spain_

Big Data engineer leading the set up, provision and deployment of Python-based solutions for Apache Spark processes.
Core concepts associated to Python deployment, including python project archetypes, libraries and runtime artifacts provision, testing support are
generated according to Datio requirements. Additionally, some of my daily tasks include the release of learning resources for Python apprentices.

Courses
-------

#### 2014: MongoDB for Developers by 10gen
Fundamentals of NoSQL databases applined on Mongo DB systems

#### 2015: MEAN Stack development fundamentals by Redradix
Basics of web development with Mongo DB, Express, Angular and Node.js on a 5-day crash course

#### 2017: Scrum Fundamentals by Scrummanager.net
Basics of Agile software management methodologies on a 2-day session presented by Javier Garzás, one of the most enthusiastic voices about Agile methodologies in Spain

Interests
---------

### Professional

 * Software design
 * Agile methods
 * User experience design
 * Big Data
 * Data Science

### Personal

 * photography
 * football
 * travel
 * music
 * painting 
 * art
 * cooking
 * biking